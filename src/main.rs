use std::{collections::HashMap, sync::{Arc, RwLock}};

use bitcoin::bip32::Xpriv;
use cashu::mint::keys::{KeySet, Keys};
use poem::{
    listener::TcpListener, middleware::AddDataEndpoint, web::{Data}, EndpointExt, Route, Server
};
use poem_openapi::{param::Path, payload::Json, ApiResponse, Object, OpenApi, OpenApiService};
use serde::{Deserialize, Serialize};

#[derive(Object, Serialize, Deserialize)]
struct GetKeysResponse {
    keysets: Vec<GetKeysResponseKeySet>,
}

#[derive(Object, Serialize, Deserialize)]
struct GetKeysetResponse {
    keysets: Vec<GetKeysetResponseKeySet>,
}


#[derive(Object, Serialize, Deserialize)]
struct GetKeysResponseKeySet {
    id: String,
    unit: String,
    keys: HashMap<u32, String>,
}

impl From<KeySet> for GetKeysResponseKeySet {
    fn from(key_set: KeySet) -> Self {
        let keys = key_set
            .keys
            .into_iter()
            .map(|(k, v)| (k, hex::encode(v)))
            .collect();
        GetKeysResponseKeySet {
            id: hex::encode(key_set.id),
            unit: key_set.unit.to_string(),
            keys,
        }
    }
}

#[derive(Object, Serialize, Deserialize)]
struct GetKeysetResponseKeySet {
    id: String,
    unit: String,
    active: bool,
}

impl From<KeySet> for GetKeysetResponseKeySet {
    fn from(key_set: KeySet) -> Self {
        GetKeysetResponseKeySet {
            id: hex::encode(key_set.id),
            unit: key_set.unit.to_string(),
            active: key_set.active,
        }
    }
}

#[derive(ApiResponse)]
enum GetKeysForIdResponse {
    #[oai(status = 200)]
    Ok(Json<GetKeysResponse>),
    #[oai(status = 400)]
    MalformedId,
    #[oai(status = 404)]
    UnknownId,
}

struct Api;
#[OpenApi]
impl Api {
    #[oai(path = "/v1/keys", method = "get")]
    async fn keys(&self, keys: Data<&Arc<RwLock<Keys>>>) -> Json<GetKeysResponse> {
        let key_sets = {
            let keys = keys.read().expect("Unable to read lock keys");
            keys.active_keysets()
        };
        let key_sets = key_sets.into_iter().map(From::from).collect();  

        Json(GetKeysResponse { keysets: key_sets })
    }
    #[oai(path = "/v1/keys/:id", method = "get")]
    async fn keys_for_id(&self, id: Path<String>, keys: Data<&Arc<RwLock<Keys>>>) -> GetKeysForIdResponse {
        let id = id.0;
        if let Ok(id) = hex::decode(id) {
            if let Ok(id) = id.try_into() {
                if let Some(key_set) = {
                    let keys = keys.read().expect("Unable to read lock keys");
                    keys.keyset_for_id(id)
                } {
                    GetKeysForIdResponse::Ok(Json(GetKeysResponse { keysets: vec![key_set.into()] }))
                } else {
                    GetKeysForIdResponse::UnknownId
                }
            } else {
                GetKeysForIdResponse::MalformedId
            }
        } else {
            GetKeysForIdResponse::MalformedId
        }
    }
    #[oai(path = "/v1/keysets", method = "get")]
    async fn keysets(&self, keys: Data<&Arc<RwLock<Keys>>>) -> Json<GetKeysetResponse> {
        let key_sets = {
            let keys = keys.read().expect("Unable to read lock keys");
            keys.keysets()
        };
        let key_sets = key_sets.into_iter().map(From::from).collect();

        Json(GetKeysetResponse { keysets: key_sets})
    }
}

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
    let (app, _) = setup_app();

    Server::new(TcpListener::bind("127.0.0.1:3000"))
        .run(app)
        .await
}

fn setup_app() -> (AddDataEndpoint<Route, Arc<RwLock<Keys>>>, Arc<RwLock<Keys>>) {
    use bitcoin::Network::Bitcoin;
    let seed: Vec<u8> = hex::decode("fffcf9f6f3f0edeae7e4e1dedbd8d5d2cfccc9c6c3c0bdbab7b4b1aeaba8a5a29f9c999693908d8a8784817e7b7875726f6c696663605d5a5754514e4b484542").unwrap();
    let m = Xpriv::new_master(Bitcoin, &seed).unwrap();
    let keys = Keys::new(m).unwrap();
    let keys = Arc::new(RwLock::new(keys));


    let api_service = OpenApiService::new(Api, "Cashu nuts", "1.0").server("http://localhost:3000");
    let ui = api_service.swagger_ui();
    let spec = api_service.spec();
    let app = Route::new()
        .nest("/", api_service)
        .nest("/docs/ui", ui)
        .at("/docs/spec", poem::endpoint::make_sync(move |_| spec.clone()))
        .data(keys.clone());
    (app, keys)
}

#[cfg(test)]
mod tests {
    use super::*;
    use cashu::mint::keys;
    use poem::{http::StatusCode, test::{TestClient, TestJsonObject}};
    const KEYSET_0_AGE_OVER_15: &str = r#"
        {
            "id": "00e6730f62d591bd",
            "unit": ">15",
            "keys":
                {
                    "1": "0290fe370c182fe60afe7420a56492e8f7f02d6b72d1900129e6f6991758d9202e"
                }
        }
        "#;
    const KEYSET_0_AGE_OVER_18: &str = r#"
        {
            "id": "00a1886c45e2fda6",
            "unit": ">18",
            "keys":
                {
                    "1": "0274ef341adf3aceed8b531e8ae0219974d2961e4ae8e1240b3323093e833471ed"
                }
        }
    "#;
    const KEYSET_0_AGE_OVER_21: &str = r#"
        {
            "id": "0083bd6f458b5985",
            "unit": ">21",
            "keys":
                {
                    "1": "02e2fea2541783cbd9f51791d1bea2d2b2b7258fd9684b0cc1ce9f875c1e865235"
                }
        }
    "#;

    const KEYSETS_1: &str = r#"
    {
        "keysets": [
            {
                "id": "00e6730f62d591bd",
                "unit": ">15",
                "active": false
            },
            {
                "id": "00a1886c45e2fda6",
                "unit": ">18",
                "active": false
            },
            {
                "id": "0083bd6f458b5985",
                "unit": ">21",
                "active": false
            },
            {
                "id": "005cd88a3149dbdf",
                "unit": ">15",
                "active": true
            },
            {
                "id": "00bb7d8d5fc691ec",
                "unit": ">18",
                "active": true
            },
            {
                "id": "008b273cd3e840d1",
                "unit": ">21",
                "active": true
            }
        ]
    }
    "#;

    #[tokio::test]
    async fn test_keys() {
        let (app, _) = setup_app();
        let cli = TestClient::new(app);

        let resp = cli.get("/v1/keys").send().await;

        resp.assert_status_is_ok();

        let json = resp.json().await;
        let json = json.value();
        let json = json.object();
        let key_sets = json.get("keysets");
        let key_sets = key_sets.object_array();
        assert_eq!(3, key_sets.len());

        key_sets.iter().for_each(|k| assert_valid_get_keys_response_key_set(k));
    }

    #[tokio::test]
    async fn test_specific_keys() {
        let (app, _) = setup_app();
        let cli = TestClient::new(app);

        let resp = cli.get("/v1/keys/00e6730f62d591bd").send().await;

        resp.assert_status_is_ok();

        let json = resp.json().await;
        let json = json.value();
        let json = json.object();
        let key_sets = json.get("keysets");
        let key_sets = key_sets.object_array();
        assert_eq!(1, key_sets.len());

        assert_get_keys_response_keyset_eq(key_sets.get(0).unwrap(), serde_json::from_str(KEYSET_0_AGE_OVER_15).unwrap());
    }

    #[tokio::test]
    async fn test_specific_keys_malformed_id() {
        let (app, _) = setup_app();
        let cli = TestClient::new(app);

        let resp = cli.get("/v1/keys/00e6730f62d591bz").send().await;
        resp.assert_status(StatusCode::BAD_REQUEST);

        let resp = cli.get("/v1/keys/00e6730f62d591bd00").send().await;
        resp.assert_status(StatusCode::BAD_REQUEST);
    }

    #[tokio::test]
    async fn test_specific_keys_unknown_id() {
        let (app, _) = setup_app();
        let cli = TestClient::new(app);

        let resp = cli.get("/v1/keys/00e6730f62d591be").send().await;
        resp.assert_status(StatusCode::NOT_FOUND);
    }

    #[tokio::test]
    async fn test_keysets() {
        let (app, keys) = setup_app();
        {
            let mut keys = keys.write().expect("Unable to acquire lock");
            keys.apply(keys::Event::PeriodStarted(1));
        }

        let cli = TestClient::new(app);

        let resp = cli.get("/v1/keysets").send().await;

        resp.assert_status_is_ok();


        let json = resp.json().await;
        let json = json.value();
        let json = json.object();
        let key_sets = json.get("keysets");
        let key_sets = key_sets.object_array();
        assert_eq!(6, key_sets.len());

        for (index, k) in key_sets.iter().enumerate() {
            assert_valid_get_keyset_response_key_set(index, k);
        } 
    }
    
    fn assert_valid_get_keys_response_key_set(k: &TestJsonObject) {
        let unit = k.get("unit");
        let unit = unit.string();
        match unit {
            ">15" => assert_get_keys_response_keyset_eq(k, serde_json::from_str(KEYSET_0_AGE_OVER_15).unwrap()),
            ">18" => assert_get_keys_response_keyset_eq(k, serde_json::from_str(KEYSET_0_AGE_OVER_18).unwrap()),
            ">21" => assert_get_keys_response_keyset_eq(k, serde_json::from_str(KEYSET_0_AGE_OVER_21).unwrap()),
            _ => panic!("Unexpected unit: {:?}", unit),
        }
    }

    fn assert_get_keys_response_keyset_eq(actual: &TestJsonObject, expected: GetKeysResponseKeySet) {
        let unit = actual.get("unit");
        let unit = unit.string();
        assert_eq!(expected.unit, unit);

        let id = actual.get("id");
        let id = id.string();
        assert_eq!(expected.id, id);

        let keys = actual.get("keys");
        let keys = keys.object();
        assert_eq!(1, keys.len());

        let key = keys.get("1");
        let key = key.string();
        assert_eq!(expected.keys.get(&1).unwrap(), key);
    }

    fn assert_valid_get_keyset_response_key_set( index: usize, k: &TestJsonObject) {
        let expected: GetKeysetResponse = serde_json::from_str(KEYSETS_1).unwrap();
        assert_get_keyset_response_keyset_eq(k, &expected.keysets[index]);
    }

    fn assert_get_keyset_response_keyset_eq(actual: &TestJsonObject, expected: &GetKeysetResponseKeySet) {
        let unit = actual.get("unit");
        let unit = unit.string();
        assert_eq!(expected.unit, unit);

        let id = actual.get("id");
        let id = id.string();
        assert_eq!(expected.id, id);

        let active = actual.get("active");
        let active = active.bool();
        assert_eq!(expected.active, active);
    }
}
