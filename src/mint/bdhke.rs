use secp256k1::PublicKey;
use secp256k1::Scalar;
use secp256k1::SecretKey;
use secp256k1::Error;

use crate::SECP256K1;

/**
 * k private key of mint (one for each amount)
 * T blinded message
 */
pub fn blind_sign(private_key_of_mint: SecretKey, blinded_message: PublicKey) -> Result<PublicKey, Error> {
    let mint_secret = Scalar::from(private_key_of_mint);
    blinded_message.mul_tweak(&SECP256K1, &mint_secret)
}


#[cfg(test)]
mod tests {

    use crate::client::bdhke::blind_message;

    use super::*;

    #[test]
    fn test_sign_blind_message() {
        assert_sign_blind_message(
            "test_message",
            "0000000000000000000000000000000000000000000000000000000000000001",
            "0000000000000000000000000000000000000000000000000000000000000001",
            "025cc16fe33b953e2ace39653efb3e7a7049711ae1d8a2f7a9108753f1cdea742b",
        );
        // private key: 7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f
        // public key:  03142715675faf8da1ecc4d51e0b9e539fa0d52fdd96ed60dbe99adb15d6b05ad9
        assert_sign_blind_message(
            "test_message",
            "0000000000000000000000000000000000000000000000000000000000000001",
            "7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f",
            "027726f0e5757b4202a27198369a3477a17bc275b7529da518fc7cb4a1d927cc0d",
        );

        assert_sign_blind_message(
            "test_message",
            "0000000000000000000000000000000000000000000000000000000000000001",
            "7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f",
            "027726f0e5757b4202a27198369a3477a17bc275b7529da518fc7cb4a1d927cc0d",
        );

        assert_sign_blinded_message(
            "020ade5d0494ee5a42b11b619bd1ac49364c3a91a549b58fa6db91f7a2a4bf2439",
            "7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f",
            "03d43aa5a3eb0fae292c4e7649806b21fe849f59316ad6dd89209d9e792235b3b5",
        );
    }

    fn assert_sign_blind_message(
        message: &str,
        blinding_factor: &str,
        private_key_of_mint: &str,
        expected: &str,
    ) {
        let message = message.as_bytes();
        let blinding_factor =
            SecretKey::from_slice(&hex::decode(blinding_factor).unwrap()).unwrap().public_key(&SECP256K1);
        let private_key_of_mint =
            SecretKey::from_slice(&hex::decode(private_key_of_mint).unwrap()).unwrap();
        let expected = hex::decode(expected).unwrap();

        assert_eq!(
            blind_sign(
                private_key_of_mint,
                blind_message(&message, blinding_factor).unwrap()
            )
            .unwrap().serialize(),
            *expected
        );
    }

    fn assert_sign_blinded_message(
        blinded_message: &str,
        private_key_of_mint: &str,
        expected: &str,
    ) {
        let blinded_message = PublicKey::from_slice(
            &hex::decode(blinded_message).unwrap(),
        ).unwrap();
        let private_key_of_mint =
            SecretKey::from_slice(&hex::decode(private_key_of_mint).unwrap()).unwrap();
        let expected = hex::decode(expected).unwrap();

        assert_eq!(
            blind_sign(
                private_key_of_mint,
                blinded_message
            )
            .unwrap().serialize(),
            *expected
        );
    }
}
