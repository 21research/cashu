use std::collections::HashMap;

/**
 * This implements nuts-01
 * https://github.com/cashubtc/nuts/blob/main/01.md
 *
 * However, we do not provide sats but age proofs so out outputs
 * will be something like that:
 *
 * {
 *   "keysets": [
 *     {
 *       "id": "009a1f293253e41e",
 *       "unit": "age_over_15",
 *       "keys": {
 *         "1": "02194603ffa36356f4a56b7df9371fc3192472351453ec7398b8da8117e7c3e104"
 *       }
 *     },
 *     {
 *       "id": "009sdfas93253e41e",
 *       "unit": "age_over_18",
 *       "keys": {
 *         "1": "02194603f2366356f4a56b7df9371fc3192472351453ec7398b8da8117e7c3e104"
 *       }
 *     },
 *     {
 *       "id": "0033dfas93253e41e",
 *       "unit": "age_over_21",
 *       "keys": {
 *         "1": "02194603f2336356f4a56b7df9371fc3192472351453ec7398b8da8117e7c3e104"
 *       }
 *     }
 *   ]
 * }
 *
 * The unit is the age attestation we are interested in and there is always one
 * single key as there is always one single possible amount for an age attestation
 * which is one. You can ask for several age attestation but each of them is one
 * single age attestation. Having an age attestation of value 2 would be strange.
 *
 * All the keys are derived from a single root seed. The seed is the only thing
 * that needs to be saved for a mint to restart operation. From that, the mint can
 * find all the leys it ever used.
 *
 * However, the currently active keyset should be saved so that the mint has an
 * idea where to restart from.
 *
 * Derivation path:
 *
 *   m / purpose' / coin_type' / account' / period / unit / amount
 *
 * m: the root key
 * purpose': 0x6d696e74' (1835626100') (See Bip-43)
 * coin_type': 0x70726976' (1886546294') (See Bip-44 and SLIP-44)
 * account': 0'
 * period: the period we are in. Monotonically increases everytime we invalidate
 *         a previous keyset.
 * unit: the unit for which to sign, for instance sat or, in our case, age_over_18
 * amout: the amount for which to sign, usually 1 in our case.
 */
use bitcoin::bip32::{ChildNumber, Error, Xpriv};
use bitcoin_hashes::{sha256, Hash};
use secp256k1::constants::PUBLIC_KEY_SIZE;
use secp256k1::PublicKey;

use crate::SECP256K1;

#[derive(Debug, PartialEq, Eq)]
pub struct KeySet {
    pub id: [u8; 8],
    pub unit: Unit,
    pub keys: HashMap<u32, [u8; PUBLIC_KEY_SIZE]>,
    pub active: bool,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Unit {
    AgeOver15 = 0x003e3135,
    AgeOver18 = 0x003e3138,
    AgeOver21 = 0x003e3230,
}

impl std::fmt::Display for Unit {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Unit::AgeOver15 => write!(f, ">15"),
            Unit::AgeOver18 => write!(f, ">18"),
            Unit::AgeOver21 => write!(f, ">21"),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Event {
    PeriodStarted(u32),
}

#[derive(Debug)] // TODO Are we sure we want to implement Clone?
pub struct Keys {
    m: Xpriv,
    active_period: usize, // Number of periods is limited in practice by the addressing capability of processor. Let's keep it that way and address it if it becomes a problem by using a different data structure to store keys.
    key_sets: Vec<UnitKeys>,
}
#[derive(Clone, Debug)]
struct UnitKeys {
    age_over_15: PublicKey,
    age_over_18: PublicKey,
    age_over_21: PublicKey,
}
impl Keys {
    pub fn new(m: Xpriv) -> Result<Keys, Error> {
        let active_period = 0;
        let (age_over_15, age_over_18, age_over_21) = derive_pub_keys(m, active_period as u32)?;
        Ok(Keys {
            m,
            active_period,
            key_sets: vec![UnitKeys {
                age_over_15,
                age_over_18,
                age_over_21,
            }],
        })
    }

    pub fn keysets(&self) -> Vec<KeySet> {
        (&self.key_sets)
            .iter()
            .enumerate()
            .map(|(i, k)| keysets(&k, i == self.active_period))
            .flatten()
            .collect()
    }

    pub fn keyset_for_id(&self, id: [u8; 8]) -> Option<KeySet> {
        for (i, k) in self.key_sets.iter().enumerate() {
            if compute_keyset_id(&k.age_over_15) == id {
                return Some(keyset(
                    Unit::AgeOver15,
                    &k.age_over_15,
                    i == self.active_period,
                ));
            } else if compute_keyset_id(&k.age_over_18) == id {
                return Some(keyset(
                    Unit::AgeOver18,
                    &k.age_over_18,
                    i == self.active_period,
                ));
            } else if compute_keyset_id(&k.age_over_21) == id {
                return Some(keyset(
                    Unit::AgeOver21,
                    &k.age_over_21,
                    i == self.active_period,
                ));
            }
        }
        return None;
    }

    pub fn active_keysets(&self) -> Vec<KeySet> {
        keysets(&self.key_sets[self.active_period], true)
    }

    pub fn start_next_period(&self) -> Result<Event, Error> {
        let next_period = self.active_period + 1;

        // The following derivation only ensures that the derivation for the
        // next period is actually possible.
        // If it fails, then we consider this command that want to move to
        // the next period as impossible and fail.
        // If it succeeds then we're fine with moving to this next period.
        // This is how we ensure that applying the returned event will never
        // fail. Indeed, applying the event will also generate the keys and
        // we consider that it will succeed if it does here
        derive_pub_keys(self.m, next_period as u32)?; //ensure we can actually derive the keys

        Ok(Event::PeriodStarted(next_period as u32))
    }

    pub fn apply(&mut self, event: Event) {
        match event {
            Event::PeriodStarted(period) => {
                let new_period = period.try_into().unwrap(); // Number of periods is limited in practice by the addressing capability of processor. Let's keep it that way and address it if it becomes a problem by using a different data structure to store keys.

                for i in self.active_period + 1..=new_period {
                    let (age_over_15, age_over_18, age_over_21) =
                        derive_pub_keys(self.m, i as u32).expect("The derivations have been validated when emitting the event so must not fail here");
                    self.key_sets.push(UnitKeys {
                        age_over_15,
                        age_over_18,
                        age_over_21,
                    });
                }
                self.active_period = new_period;
            }
        }
    }

    pub fn hydrate(&mut self, events: Vec<Event>) {
        events.iter().for_each(|e| self.apply(*e));
    }
}

fn derive_pub_keys(m: Xpriv, period: u32) -> Result<(PublicKey, PublicKey, PublicKey), Error> {
    Ok((
        derive_pub_key(m, period, Unit::AgeOver15 as u32)?,
        derive_pub_key(m, period, Unit::AgeOver18 as u32)?,
        derive_pub_key(m, period, Unit::AgeOver21 as u32)?,
    ))
}

fn derive_pub_key(seed: Xpriv, period: u32, unit: u32) -> Result<PublicKey, Error> {
    let purpose = ChildNumber::from_hardened_idx(0x6d696e74)?;
    let coin_type = ChildNumber::from_hardened_idx(0x70726976)?;
    let account = ChildNumber::from_hardened_idx(0)?;
    let period = ChildNumber::from_normal_idx(period)?;
    let unit = ChildNumber::from_normal_idx(unit)?;
    let amount = ChildNumber::from_normal_idx(1)?;

    Ok(seed
        .derive_priv(
            &SECP256K1,
            &[purpose, coin_type, account, period, unit, amount],
        )?
        .private_key
        .public_key(&SECP256K1))
}

fn compute_keyset_id(key: &PublicKey) -> [u8; 8] {
    let key = key.serialize();
    let hash = sha256::Hash::hash(&key).to_byte_array();
    let mut keyset_id = [0u8; 8];
    keyset_id[0] = 0;
    keyset_id[1..].copy_from_slice(&hash[..7]);
    keyset_id
}

fn keysets(keys: &UnitKeys, active: bool) -> Vec<KeySet> {
    vec![
        keyset(Unit::AgeOver15, &keys.age_over_15, active),
        keyset(Unit::AgeOver18, &keys.age_over_18, active),
        keyset(Unit::AgeOver21, &keys.age_over_21, active),
    ]
}

fn keyset(unit: Unit, key: &PublicKey, active: bool) -> KeySet {
    KeySet {
        id: compute_keyset_id(key),
        unit,
        keys: vec![(1, key.serialize())].into_iter().collect(),
        active,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use bitcoin::Network::Bitcoin;
    use pretty_assertions::assert_eq;
    use Unit::*;

    #[test]
    fn test_derive_pub_key() {
        let m = test_seed();
        let period = 0;

        // m/1835626100'/1886546294'/0'/0/4075829
        assert_derivation(
            m,
            period,
            AgeOver15 as u32,
            "0290fe370c182fe60afe7420a56492e8f7f02d6b72d1900129e6f6991758d9202e",
        );

        // m/1835626100'/1886546294'/0'/0/4075832
        assert_derivation(
            m,
            period,
            AgeOver18 as u32,
            "0274ef341adf3aceed8b531e8ae0219974d2961e4ae8e1240b3323093e833471ed",
        );

        // m/1835626100'/1886546294'/0'/0/4076080
        assert_derivation(
            m,
            period,
            AgeOver21 as u32,
            "02e2fea2541783cbd9f51791d1bea2d2b2b7258fd9684b0cc1ce9f875c1e865235",
        );
    }

    fn assert_derivation(m: Xpriv, period: u32, unit: u32, expected: &str) {
        let expected = hex::decode(expected).unwrap();
        let pub_key = derive_pub_key(m, period, unit).unwrap();
        assert_eq!(pub_key.serialize(), *expected);
    }

    #[test]
    fn test_active_keyset() {
        let keys = Keys::new(test_seed()).unwrap();

        assert_eq!(
            keys.active_keysets(),
            vec![
                expected_age_over_15_keyset(0, true),
                expected_age_over_18_keyset(0, true),
                expected_age_over_21_keyset(0, true),
            ]
        );
    }

    #[test]
    fn test_active_keyset_for_some_period() {
        let mut keys = Keys::new(test_seed()).unwrap();
        keys.apply(Event::PeriodStarted(1));

        assert_eq!(
            keys.active_keysets(),
            vec![
                expected_age_over_15_keyset(1, true),
                expected_age_over_18_keyset(1, true),
                expected_age_over_21_keyset(1, true),
            ]
        );
    }

    #[test]
    fn test_can_start_a_new_period() {
        let keys = Keys::new(test_seed()).unwrap();

        assert_eq!(Ok(Event::PeriodStarted(1)), keys.start_next_period());
    }

    fn test_seed() -> Xpriv {
        // See https://iancoleman.io/bip39/
        let seed: Vec<u8> = hex::decode("fffcf9f6f3f0edeae7e4e1dedbd8d5d2cfccc9c6c3c0bdbab7b4b1aeaba8a5a29f9c999693908d8a8784817e7b7875726f6c696663605d5a5754514e4b484542").unwrap();
        Xpriv::new_master(Bitcoin, &seed).unwrap()
    }

    #[test]
    fn test_keysets() {
        let mut keys = Keys::new(test_seed()).unwrap();
        keys.apply(Event::PeriodStarted(1));

        assert_eq!(
            keys.keysets(),
            vec![
                expected_age_over_15_keyset(0, false),
                expected_age_over_18_keyset(0, false),
                expected_age_over_21_keyset(0, false),
                expected_age_over_15_keyset(1, true),
                expected_age_over_18_keyset(1, true),
                expected_age_over_21_keyset(1, true),
            ]
        );
    }

    #[test]
    fn test_keyset_for_id() {
        let mut keys = Keys::new(test_seed()).unwrap();
        keys.apply(Event::PeriodStarted(1));

        let key_set = keys
            .keyset_for_id(hex::decode("00e6730f62d591bd").unwrap().try_into().unwrap())
            .unwrap();

        assert_eq!(key_set, expected_age_over_15_keyset(0, false));
    }

    fn expected_age_over_15_keyset(period: usize, active: bool) -> KeySet {
        let (id, key) = match period {
            0 => (
                "00e6730f62d591bd",
                "0290fe370c182fe60afe7420a56492e8f7f02d6b72d1900129e6f6991758d9202e",
            ),
            1 => (
                "005cd88a3149dbdf",
                "0262d38790fba63b4441e7cf13321701f2a508e34893ddbbfd6b3b50706424bd7b",
            ),
            _ => panic!("Unkown period for tests"),
        };
        expected_keyset(id, AgeOver15, key, active)
    }

    fn expected_age_over_18_keyset(period: usize, active: bool) -> KeySet {
        let (id, key) = match period {
            0 => (
                "00a1886c45e2fda6",
                "0274ef341adf3aceed8b531e8ae0219974d2961e4ae8e1240b3323093e833471ed",
            ),
            1 => (
                "00bb7d8d5fc691ec",
                "0241d1b3b228f7e4e5cabe4d5945d0be59ffa7ad5769b96a92aae19a3255b58e7d",
            ),
            _ => panic!("Unkown period for tests"),
        };
        expected_keyset(id, AgeOver18, key, active)
    }

    fn expected_age_over_21_keyset(period: usize, active: bool) -> KeySet {
        let (id, key) = match period {
            0 => (
                "0083bd6f458b5985",
                "02e2fea2541783cbd9f51791d1bea2d2b2b7258fd9684b0cc1ce9f875c1e865235",
            ),
            1 => (
                "008b273cd3e840d1",
                "03ab3c5157851a5f0afaacbf0f7e859b0196da807775131c2a49ce46fb155bdbde",
            ),
            _ => panic!("Unkown period for tests"),
        };
        expected_keyset(id, AgeOver21, key, active)
    }

    fn expected_keyset(id: &str, unit: Unit, key: &str, active: bool) -> KeySet {
        KeySet {
            id: hex::decode(id).unwrap().try_into().unwrap(),
            unit,
            keys: vec![(1, hex::decode(key).unwrap().try_into().unwrap())]
                .into_iter()
                .collect(),
            active,
        }
    }
}
