use lazy_static::lazy_static;
use secp256k1::{All, Secp256k1};

pub mod client;
pub mod mint;

lazy_static! {
    static ref SECP256K1: Secp256k1<All> = Secp256k1::new();
}