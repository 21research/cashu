use bitcoin_hashes::sha256;
use bitcoin_hashes::Hash;
use secp256k1::PublicKey;
use secp256k1::Scalar;
use secp256k1::SecretKey;
use secp256k1::{Error, XOnlyPublicKey};

use crate::SECP256K1;

/**
 * x random string (secret message), corresponds to point Y on curve
 * Y point on curve
 * r private key (blinding factor)
 */
pub fn blind_message(message: &[u8], blinding_factor: PublicKey) -> Result<PublicKey, Error> {
    let point_on_curve = hash_to_curve(message);
    point_on_curve
        ?.combine(&blinding_factor)
}

/**
 * C_ = kB_ blinded key
 * r private key (blinding factor)
 * K public key of mint
 * C_ - rK unblinded key
 */
pub fn unblind_message(
    blinded_key: PublicKey,
    blinding_factor: SecretKey,
    public_key_of_mint: PublicKey,
) -> Result<PublicKey, Error> {
    let blinding_factor = Scalar::from(blinding_factor);
    let blinding_factor_mul_public_key_of_mint = public_key_of_mint
        .mul_tweak(&SECP256K1, &blinding_factor)?;

    blinded_key.combine(&blinding_factor_mul_public_key_of_mint.negate(&SECP256K1))
}

const DOMAIN_SEPARATOR: &[u8; 28] = b"Secp256k1_HashToCurve_Cashu_";

fn hash_to_curve(msg: &[u8]) -> Result<PublicKey, Error> {
    let msg_to_hash: Vec<u8> = [DOMAIN_SEPARATOR, msg].concat();
    let msg_hash: [u8; 32] = sha256::Hash::hash(&msg_to_hash).to_byte_array();

    let mut counter = 0;
    loop {
        match key_from_hash_and_counter(msg_hash, counter) {
            Ok(p) => break Ok(PublicKey::from_x_only_public_key(p, secp256k1::Parity::Even)),
            Err(e) => if counter < 2_u32.pow(16) {
                counter = counter + 1
            } else {
                break Err(e);
            }
        }
    }
}

fn key_from_hash_and_counter(msg_hash: [u8; 32], counter: u32) -> Result<XOnlyPublicKey, Error> {
    let mut bytes_to_hash = Vec::with_capacity(36);
    bytes_to_hash.extend_from_slice(&msg_hash);
    bytes_to_hash.extend_from_slice(&counter.to_le_bytes());

    let hash: [u8; 32] = sha256::Hash::hash(&bytes_to_hash).to_byte_array();

    XOnlyPublicKey::from_slice(&hash)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hash_to_curve() {
        assert_hash_to_curve(
            "0000000000000000000000000000000000000000000000000000000000000000",
            "024cce997d3b518f739663b757deaec95bcd9473c30a14ac2fd04023a739d1a725",
        );
        assert_hash_to_curve(
            "0000000000000000000000000000000000000000000000000000000000000001",
            "022e7158e11c9506f1aa4248bf531298daa7febd6194f003edcd9b93ade6253acf",
        );
        assert_hash_to_curve(
            "0000000000000000000000000000000000000000000000000000000000000002",
            "026cdbe15362df59cd1dd3c9c11de8aedac2106eca69236ecd9fbe117af897be4f",
        );
    }

    #[test]
    fn test_blind_message() {
        assert_blind_message(
            "d341ee4871f1f889041e63cf0d3823c713eea6aff01e80f1719f08f9e5be98f6",
            "99fce58439fc37412ab3468b73db0569322588f62fb3a49182d67e23d877824a",
            "033b1a9737a40cc3fd9b6af4b723632b76a67a36782596304612a6c2bfb5197e6d",
        );
        assert_blind_message(
            "f1aaf16c2239746f369572c0784d9dd3d032d952c2d992175873fb58fae31a60",
            "f78476ea7cc9ade20f9e05e58a804cf19533f03ea805ece5fee88c8e2874ba50",
            "029bdf2d716ee366eddf599ba252786c1033f47e230248a4612a5670ab931f1763",
        );
        assert_blind_message2(
            "c031acce93e5869ea61125cf64bcdc1f89cffda8efee94657e9609c27accdf1d",
            "02686a3077dee57aae98f1544898eebd370f9ef17fa4d0226316cc2f8f856033fe",
            "020ade5d0494ee5a42b11b619bd1ac49364c3a91a549b58fa6db91f7a2a4bf2439",
        );
    }

    #[test]
    fn test_unblind_message() {
        assert_unblind_message(
            "02a9acc1e48c25eeeb9289b5031cc57da9fe72f3fe2861d264bdc074209b107ba2",
            "0000000000000000000000000000000000000000000000000000000000000001",
            "020000000000000000000000000000000000000000000000000000000000000001",
            "03c724d7e6a5443b39ac8acf11f40420adc4f99a02e7cc1b57703d9391f6d129cd"
        );
        assert_unblind_message(
            "03d43aa5a3eb0fae292c4e7649806b21fe849f59316ad6dd89209d9e792235b3b5",
            "779eba4413147e4a1af1f97fbb1f4b8259763e405153bafbe9f92f94a6cdae50",
            "03142715675faf8da1ecc4d51e0b9e539fa0d52fdd96ed60dbe99adb15d6b05ad9",
            "024aac7d9c17020da7a926f23f32661484663560f8d871800cc51ec9d42d9af177"
        );
    }

    fn assert_unblind_message(blinded_signed_message: &str, blinded_factor: &str, public_key_of_mint: &str, expected: &str) {
        let blind_signed_message = PublicKey::from_slice(&hex::decode(blinded_signed_message).unwrap()).unwrap();
        let blinding_factor = SecretKey::from_slice(&hex::decode(blinded_factor).unwrap()).unwrap();
        let public_key_of_mint = PublicKey::from_slice(&hex::decode(public_key_of_mint).unwrap()).unwrap();
    
        let result = unblind_message(blind_signed_message, blinding_factor, public_key_of_mint);
    
        let expected = hex::decode(expected).unwrap();
        assert_eq!(result.unwrap().serialize(), *expected);
    }
    
    fn assert_hash_to_curve(message: &str, expected_point_on_curve: &str) {
        assert_eq!(
            hash_to_curve(&hex::decode(message).unwrap()).unwrap().serialize(),
            *hex::decode(expected_point_on_curve).unwrap()
        );
    }

    fn assert_blind_message(message: &str, blinding_factor: &str, expected: &str) {
        let message = hex::decode(message).unwrap();
        let blinding_factor =
            SecretKey::from_slice(&hex::decode(blinding_factor).unwrap()).unwrap().public_key(&SECP256K1);
        let expected = hex::decode(expected).unwrap();

        assert_eq!(
            blind_message(&message, blinding_factor).unwrap().serialize(),
            *expected
        );
    }
    
    fn assert_blind_message2(message: &str, blinding_factor: &str, expected: &str) {
        let message = hex::decode(message).unwrap();
        let blinding_factor =
            PublicKey::from_slice(&hex::decode(blinding_factor).unwrap()).unwrap();
            // SecretKey::from_slice(&hex::decode(blinding_factor).unwrap()).unwrap().public_key(&SECP256K1);
        let expected = hex::decode(expected).unwrap();

        assert_eq!(
            blind_message(&message, blinding_factor).unwrap().serialize(),
            *expected
        );
    }
}
